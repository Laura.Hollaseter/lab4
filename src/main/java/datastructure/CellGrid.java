package datastructure;

import cellular.CellState;
import java.util.ArrayList;
import java.util.Arrays;


public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    //private Array<CellState> initialState;
    private CellState[][]cellGrid;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        cellGrid = new CellState[rows][columns];
        
        for (int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                cellGrid[i][j] = initialState;
            }

        }

    }

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // row = num rows
        
        //else; ThrowMeIllegalArgument

        cellGrid[row][column] = element;
        
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {

        IGrid CopyGrid = new CellGrid(rows, columns, CellState.DEAD);
        for (int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                CopyGrid.set(i, j, get(i, j)); 
            }
        }
        return CopyGrid;


    }
    
}
